package main

import (
	"github.com/guilhermevarela/golang/02_package/icomefromalaska"
	"github.com/guilhermevarela/golang/02_package/stringutil"
	"fmt"
)
func main() {
	fmt.Println(stringutil.Reverse("!oG ,olleH"))
	fmt.Println(stringutil.MyName)
	fmt.Println(willieecoyote.CoyoteName)
}
