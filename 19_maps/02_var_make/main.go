package main

import "fmt"

func main() {
	var greeting = make(map[string]string)
	greeting["Tim"] = "Good morning"
	greeting["Jenny"] = "Bonjour"
	fmt.Println(greeting)
	fmt.Println(greeting ==nil)

}

