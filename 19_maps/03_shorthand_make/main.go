package main

import "fmt"

func main() {
	greeting := make(map[string]string)
	greeting["Tim"] = "Good morning"
	greeting["Jenny"] = "Bonjour"
	fmt.Println(greeting)
	fmt.Println(greeting ==nil)

}

