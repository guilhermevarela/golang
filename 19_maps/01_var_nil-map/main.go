package main

import "fmt"

func main() {
	var greeting map[string]string
	fmt.Println(greeting)
	fmt.Println(greeting ==nil)

}

//Add these lines:
/*
	greeting["Tim"] = "Good morning"
	greeting["Jenny"] = "Bonjour"
*/
// and you will get this:
// panic: assignment to entry in nil map
