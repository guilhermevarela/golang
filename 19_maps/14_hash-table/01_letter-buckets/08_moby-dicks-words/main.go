package main

import (
	"bufio"
	"fmt"
	"os"
	"net/http"
	"log"
)

func main() {

	res, err := http.Get("http://www.gutenberg.org/files/2701/old/moby10b.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()

	//scan the page
	//NewScanner takles a reader and res.Body implements the reader interface
	scanner := bufio.NewScanner(res.Body)
	// Set the split function for the scanning operation.
	scanner.Split(bufio.ScanWords)
	// Count the words.
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading input:", err)
	}
}