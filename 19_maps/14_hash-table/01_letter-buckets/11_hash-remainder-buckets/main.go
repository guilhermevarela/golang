package main

import (
	"bufio"
	"fmt"
	"log"
	"net/http"
)


func main() {
	//get the book moby dick
	res, err := http.Get("http://www.gutenberg.org/files/2701/old/moby10b.txt")
	if err != nil {
		log.Fatal(err)
	}

	//scan the page
	scanner := bufio.NewScanner(res.Body)
	defer res.Body.Close()

	//Set the split function for scanning operation.
	scanner.Split(bufio.ScanWords)
	//Create slice to hold counts
	buckets := make([]int, 12)
	//Loop over words
	for scanner.Scan() {
		n := HashBucket(scanner.Text(), 12)
		buckets[n] ++
	}
	fmt.Println(buckets)
	//fmt.Println(buckets[65:123])
	//fmt.Println(buckets)
	//fmt.Println("**************")
	//for i := 28; i <= 126; i++ {
	//	fmt.Printf("%v - %c -%v \n", i,i, buckets[i])
	//}

}

func HashBucket(word string, buckets int) int {
	letter :=  int(word[0])
	bucket := letter % buckets

	return bucket
}
