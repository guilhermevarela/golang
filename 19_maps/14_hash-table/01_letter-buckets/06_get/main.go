package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	res, err := http.Get("http://www.gutenberg.org/files/2701/old/moby10b.txt")
	if err != nil {
		log.Fatalln(err)
	}
	bs, err := ioutil.ReadAll(res.Body); if err != nil {
		log.Fatalln(err)
	}
	defer res.Body.Close()

	fmt.Printf("%s", bs)

}

