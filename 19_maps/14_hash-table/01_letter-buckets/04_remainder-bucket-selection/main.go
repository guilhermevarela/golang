package main

import "fmt"



func main() {
	word := "Hello"
	letter := rune(word[0])
	fmt.Println(letter)

	for i := 65; i <= 122 ; i++ {
		fmt.Println(i, " - ", string(i), " - ", i % 12)
	}

}
