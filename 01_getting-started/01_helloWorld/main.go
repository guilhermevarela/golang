package main

import "fmt"

func main() {
	n, err := fmt.Println("Hello world")
	if err != nil {
		fmt.Println("Println error:", n)
		return
	}
}
