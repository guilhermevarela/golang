package main

import (
	"fmt"
	"encoding/json"
)

type Person struct {
	First string
	Last  string `json:"-"`
	Age   int `json:"wisdom score"`
}


func main() {
	p := Person{
			First:"James",
			Last:"Bond",
			Age: 36,
		}

	bs, _ := json.Marshal(p)
	fmt.Println(bs)
	fmt.Printf("%T \n", bs)
	fmt.Println(string(bs))
}