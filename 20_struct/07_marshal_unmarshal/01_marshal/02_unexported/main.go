package main

import (
	"fmt"
	"encoding/json"
)

type person struct {
	first string
	last  string
	age   int
}


func main() {
	p := person{
			first:"James",
			last:"Bond",
			age: 36,
		}
	fmt.Println(p)

	bs, _ := json.Marshal(p)
	fmt.Println(bs)
	fmt.Printf("%T \n", bs)
	fmt.Println(string(bs))
}