package main

import (
	"fmt"
	"encoding/json"
)

type person struct {
	First string
	Last  string
	Age   int
	notExported int
}


func main() {
	p := person{
			First:"James",
			Last:"Bond",
			Age: 36,
		}

	bs, _ := json.Marshal(p)
	fmt.Println(bs)
	fmt.Printf("%T \n", bs)
	fmt.Println(string(bs))
}