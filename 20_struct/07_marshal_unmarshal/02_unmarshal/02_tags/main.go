package main

import (
	"fmt"
	"encoding/json"
)

type Person struct {
	First string
	Last  string
	Age   int `json:"wisdom score"`
}


func main() {
	var p1 Person


	bs  := []byte(`{"First":"James","Last":"Bond","wisdom score": 36}`)

	json.Unmarshal(bs, &p1)
	fmt.Println("----------------------------------------")
	fmt.Println(p1.First)
	fmt.Println(p1.Last)
	fmt.Println(p1.Age)
	fmt.Printf("%T \n", p1)
	fmt.Println("----------------------------------------")
}