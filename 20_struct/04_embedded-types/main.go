package main

import (
	"fmt"

)

type Person struct {
	First string
	Last  string
	Age   int
}

func (p Person) fullName()  string {
	return p.First + " " + p.Last
}


type doubleZero struct {
	Person
	LicenseToKill bool
}

func main() {
	p1 := doubleZero{
		Person: Person{
			First:"James",
			Last:"Bond",
			Age: 36,
		},
		LicenseToKill: true,
	}
	p2 := doubleZero{
		Person: Person{
			First:"Miss",
			Last:"Moneypenny",
			Age: 52,
		},
		LicenseToKill: false,

	}
	fmt.Println(p1.First, p1.Last, p1.Age, p1.LicenseToKill, p1.fullName())
	fmt.Println(p2.First, p2.Last, p2.Age, p2.LicenseToKill, p2.fullName())
}