package main

import "fmt"

type person struct {
	name string
	age   int
}

func main() {
	p := &person{"James Bond", 36}

	fmt.Println(p)
	fmt.Printf("%T\n", p)
	fmt.Println(p.name)
	fmt.Println(p.age)


}
