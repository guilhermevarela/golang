package main

import (
	"os"
	"encoding/json"

)

type Person struct {
	First string
	Last  string
	Age   int
	notExported int
}


func main() {
	p := Person{
			First:"James",
			Last:"Bond",
			Age: 36,
			notExported: 007,
		}

	json.NewEncoder(os.Stdout).Encode(p)

}