package main

import (
	"fmt"
)

type Person struct {
	First string
	Last  string
	Age   int
}

func (p Person) greeting()  {
	fmt.Println("Hello world")
}


type doubleZero struct {
	Person
	First string
	LicenseToKill bool

}

func (dz doubleZero) greeting()  {
	fmt.Println("Miss Moneypenny, good to see you")
}

func main() {
	p1 := doubleZero{
		Person: Person{
			First:"James",
			Last:"Bond",
			Age: 36,
		},
		First: "007",
		LicenseToKill: true,
	}
	p2 := doubleZero{
		Person: Person{
			First:"Miss",
			Last:"Moneypenny",
			Age: 52,
		},
		First: "If looks could kill",
		LicenseToKill: false,

	}
	p1.greeting()
	p2.Person.greeting()
	/*fmt.Println(p1.greeting())
	fmt.Println(p2.Person.greeting())*/
}