package main

import (
	"fmt"
)

type Person struct {
	First string
	Last  string
	Age   int
}

func (p Person) fullName()  string {
	return p.First + " " + p.Last
}


type doubleZero struct {
	Person
	First string
	LicenseToKill bool

}

func main() {
	p1 := doubleZero{
		Person: Person{
			First:"James",
			Last:"Bond",
			Age: 36,
		},
		First: "007",
		LicenseToKill: true,
	}
	p2 := doubleZero{
		Person: Person{
			First:"Miss",
			Last:"Moneypenny",
			Age: 52,
		},
		First: "If looks could kill",
		LicenseToKill: false,

	}
	fmt.Println(p1.First, p1.Person.First)
	fmt.Println(p2.First, p2.Person.First)
}