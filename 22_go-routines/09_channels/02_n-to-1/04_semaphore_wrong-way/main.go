package main


import  (
	"fmt"
)

func main() {

	c := make(chan int)
	done := make(chan bool)


	go func() {
		for i := 0; i < 10;  i++ {
			c <- 2*i
		}
		done <- true

	}()

	go func() {
		for i := 0; i < 10;  i++ {
			c <- 2*i+1
		}
		done <- true

	}()

	// we block here until done <- true
	<- done // holds until there is something to be taken of channel done
	<- done // holds until there is something to be taken of channel done
	close(c)
	// to unblock above
	// we need to take values off of chan c here
	// but we never get here, because we're blocked above
	for n := range c {
		fmt.Println(n)
	}

}



/*
go run -race main.go
fatal error: all goroutines are asleep - deadlock!

 */