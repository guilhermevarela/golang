package main


import  (
	"fmt"
	"time"
)
//BATON RACE DYNAMICS -> RELAY RACE
func main() {
	//Unbuffered channel.: one variable only
	c := make(chan int)
	go func() {
		for i := 0; i < 10;  i++ {
			c <- i
			//AFTER THAT LINE EXECUTION HALTS UNTIL SOMEONE CONSUMES THAT INFO
		}
	}()
	go func() {
		for  {
			fmt.Println(<-c)
			//AFTER THAT LINE EXECUTION HALTED go-routines resume
		}
	}()

	time.Sleep(time.Second)

}



