package main


import  (
	"fmt"
)
//BATON RACE DYNAMICS -> RELAY RACE
func main() {
	//Unbuffered channel.: one variable only
	c := make(chan int)
	go func() {
		for i := 0; i < 10;  i++ {
			c <- i
			//AFTER THAT LINE EXECUTION HALTS UNTIL SOMEONE CONSUMES THAT INFO
		}
		close(c)
	}()

	for n := range c {
		fmt.Println(n)
	}

}



