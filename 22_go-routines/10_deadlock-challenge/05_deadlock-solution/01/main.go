package main

import (
	"fmt"
)

func main() {
	c := make(chan int)

	go func() {
		for i := 0; i < 10; i++ {
			c <- i
		}
	}()
	for i := 0; i < 10; i++ {
		fmt.Println(<-c)
	}
}

/* 
Why does this only print zero? 
Because zero is the first, of a total of ten, channel input
Since there's only one output, we only get to see the first
// And what can you do to get it to print all 0 - 9 numbers? Either:
i. Repeat last instruction +9 times
ii. Make a 10-sized loop printing the other 9 inputs
*/