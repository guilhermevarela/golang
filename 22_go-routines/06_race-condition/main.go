package main


import  (
	"fmt"
	"sync"
	"time"
	"math/rand"
)


var wg sync.WaitGroup
var counter int


func main() {
	wg.Add(2)
	go incrementor("Foo:")
	go incrementor("Bar:")
	wg.Wait()
}

func incrementor(s string) {
	for i := 0 ; i < 20; i++ {
		x := counter
		x ++
		time.Sleep(time.Duration(rand.Intn(3))  * time.Millisecond)
		counter = x
		fmt.Println(s ,i, "Counter:", counter)
	}
	wg.Done()
}

/*
	RUN THIS WITH THE FOLLOWING COMMAND:
	go run -race main.go
  Found 1 data race(s)
  exit status 66

 */

