package main

import "fmt"

func main() {
	name := `Varela`
	fmt.Println(name) // `Varela`

	changeMe(name)

	fmt.Println(name) // `Varela`

}

func changeMe(z string) {

	fmt.Println(z) 				//36
	z = "Rocky"
	fmt.Println(z) 				//27
}


