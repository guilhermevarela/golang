package main

import "fmt"

func main() {
	age := 36
	changeMe(age)
	fmt.Println(age) //36
}

func changeMe(z int) {
	fmt.Println(z)
	z = 27
}

//when changeMe is called on line 7
//the value 36 is being passed as an argument