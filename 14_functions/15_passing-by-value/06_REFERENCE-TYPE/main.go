package main

import "fmt"

func main() {
	m := make([]string, 1, 25)
	fmt.Println(m)
	changeMe(m)
	fmt.Println(m) //[Varela]
}

//m's address are automaticaly being passed
func changeMe(z []string) {
	z[0] = `Varela`
	fmt.Printf("z[0] %v\n",z[0]) //[Varela]
	fmt.Printf("z %v\n",z) //[Varela]
	fmt.Printf("&z %v\n",&z)
}

/* SLICES are reference types which means that they implicitly point to
an underlying array. It' a pointer to underlying array, with length and capacity
Reference types are data structures that are implicitly pointers

// https://golang.org/doc/effective_go.html#allocation_make

"SLICE", for example, is a three-item descriptor containing
a pointer to the data (inside an array), the length, and the capacity, and until those items are initialized,
the slice is nil. For slices, maps, and channels, make initializes the internal data structure and prepares
the value for use.
*/