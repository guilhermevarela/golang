package main

import "fmt"

func main() {
	age := 36
	fmt.Println("Inital age address is:",&age) // 0xc42000a280

	changeMe(&age)

	fmt.Println("Final age address is:",&age ) // 0xc42000a280
	fmt.Println("Final age is:",age ) //27
}

func changeMe(z *int) {
	fmt.Println("changeMe age's address is:",z ) // 0xc42000a280
	fmt.Println("changeMe  age is:", *z ) 				//36
	*z = 27
	fmt.Println("changeMe age's address is:",z ) // 0xc42000a280
	fmt.Println("changeMe  age is:", *z ) 				//27

}

//when changeMe is called on line 7
//the value 36 is being passed as an argument