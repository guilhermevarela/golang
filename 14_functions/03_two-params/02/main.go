package main

import "fmt"

func main() {
	greet("Dany", "Targaryen")
}

func greet(fname, lname string) {
	fmt.Println(fname, lname)
}

//greet is declared with two params
//when calling greet, pass in two arguments