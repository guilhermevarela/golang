package main

import "fmt"

//Arugments are instances of parameters
func main() {

	//arguments: "Dany", "Targaryen"
	greet("Dany", "Targaryen")
}

//parameters: fname string, lname string
func greet(fname string, lname string) {
	fmt.Println(fname, lname)
}

