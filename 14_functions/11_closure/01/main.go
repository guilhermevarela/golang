package main

import "fmt"

func main() {
	x := 36
	fmt.Println(x)
	{
		fmt.Println("x defined in outer closure:", x)
		y := "The credit belongs with the one who is in the ring"
		fmt.Println("y defined in inner closure:", y)
	}
	//fmt.Println(y)
}
