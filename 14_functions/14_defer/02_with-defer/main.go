package main


import "fmt"

func hello() {
	fmt.Print("hello ")
}

func world() {
	fmt.Println("world")
}

func main() {
	//runs right before main() exists
	defer world()
	hello()
}