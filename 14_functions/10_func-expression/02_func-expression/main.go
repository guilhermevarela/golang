package main

import "fmt"

func main() {
	//function expression .; func() anonymous expression
	greeting := func() {
		fmt.Println("Hello world!")
	}

	greeting()
	fmt.Printf("greeting type is %T\n", greeting)
}
