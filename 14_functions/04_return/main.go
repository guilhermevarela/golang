package main

import "fmt"

//Arugments are instances of parameters
func main() {

	//arguments: "Dany", "Targaryen"
	fmt.Println(greet("Dany", "Targaryen"))
}

//parameters: fname string, lname string
func greet(fname,lname string) string {
	return fmt.Sprint(fname, lname)
}

