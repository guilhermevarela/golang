package main

import "fmt"

//Arugments are instances of parameters
func main() {

	//arguments: "Dany", "Targaryen"
	fmt.Println(greet("Dany", "Targaryen"))
}

//parameters: fname string, lname string
func greet(fname,lname string) (s string) {
	s =  fmt.Sprint(fname, lname)
	return
}

/*
IMPORTANT
Avoid using named returns.
Occasionally named returns are useful. Read this article for more information:
https://www.goinggo.net/2013/10/functions-and-naked-returns-in-go.html
*/