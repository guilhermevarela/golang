package main

import "fmt"

//const p string = "death & taxes"
const p  = "death & taxes"

func main() {
	//const q  = 36
	const q int = 36

	fmt.Println("p - ", p)
	fmt.Println("q - ", q)
}
