package main

import "fmt"

const (
	a = iota // 0
	b // 1
	c // 2
)

const (
	d = iota // 0
	e // 1
	f // 2
)


func main() {
	fmt.Println("a:", a)
	fmt.Println("b:", b)
	fmt.Println("c:", c)
	fmt.Println("d:", d)
	fmt.Println("e:", e)
	fmt.Println("f:", f)
}
