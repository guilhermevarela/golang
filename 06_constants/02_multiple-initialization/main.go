package main

import "fmt"

const (
	Pi = 3.14
	Language = "Go"
)

func main() {
	fmt.Printf("Pi  %v \t %T\n", Pi, Pi)
	fmt.Printf("Language  %v \t %T\n", Language, Language)
}