package main

import "fmt"

func main() {

	a := 36

	fmt.Println("a's value: ",a) 	   // 36
	fmt.Println("a's memory address: ",&a)  //0x20818a220
	fmt.Printf("a's type: %T\n",a)
	fmt.Printf("a's memory address type: %T\n",&a)

	var b *int = &a
	fmt.Println("b = &a: b's value:", b) //0x20818a220
	fmt.Printf("b = &a: b's type:%T\n", b) //*int
	fmt.Println("*b .: value pointed by b:", *b) //36

	*b = 37 // b says: "Change the value at this address to 37"
	fmt.Println("*b = 37 .: New value of a:", a)

	// this is useful
	// we can pass a memory address instead of a bunch of values (we can pass a reference)
	// and then we can still change the value of whatever is stored at that memory address
	// this makes our programs more performant
	// we don't have to pass around large amounts of data
	// we only have to pass around addresses

	// everything is PASS BY VALUE in go, btw
	// when we pass a memory address, we are passing a value

}
