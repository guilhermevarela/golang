package main

import "fmt"

func main() {

	a := 36

	fmt.Println("a's value: ",a)
	fmt.Println("a's memory address: ",&a)
	fmt.Printf("a's type: %T\n",a)
	fmt.Printf("a's memory address type: %T\n",&a)

	var b = &a
	fmt.Println("b = &a: b's value:", b)
	fmt.Printf("b = &a: b's type:%T\n", b)

//the above code makes b a pointer to the memeory address where an int is stored
//b is of type "int pointer
//*int -- the * is part of the type --b is of type *int

}
