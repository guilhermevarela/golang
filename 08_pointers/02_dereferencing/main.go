package main

import "fmt"

func main() {

	a := 36

	fmt.Println("a's value: ",a)
	fmt.Println("a's memory address: ",&a)
	fmt.Printf("a's type: %T\n",a)
	fmt.Printf("a's memory address type: %T\n",&a)

	var b = &a
	fmt.Println("b = &a: b's value:", b)
	fmt.Printf("b = &a: b's type:%T\n", b)
	fmt.Println("*b .: value pointed by b:", *b)

	// b is an int pointer;
	// b points to the memory address where an int is stored
	// to see the value in that memory address, add a * in front of b
	// this is known as dereferencing
	// the * is an operator in this case

}
