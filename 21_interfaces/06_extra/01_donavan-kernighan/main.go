package main

import (
		"fmt"
		//"os"
		//"io/ioutil"
	"io"
	"os"
	"bytes"
)

func main() {
	fmt.Println("----------------------------")
	fmt.Println("var w io.Writer")
	var w io.Writer
	fmt.Printf("Type %T \t Value %v\n", w, w)
	fmt.Println("----------------------------")
	fmt.Println("w = os.Stdout")
	w = os.Stdout
	fmt.Printf("Type %T \t Value %v\n", w, w)
	fmt.Println("----------------------------")
	fmt.Println("w = new(bytes.Buffer)")
	w = new(bytes.Buffer)
	fmt.Printf("Type %T \t Value %v\n", w, w)
	fmt.Println("----------------------------")
	fmt.Println("w = nil")
	w = nil
	fmt.Printf("Type %T \t Value %v\n", w, w)
	fmt.Println("----------------------------")

}


