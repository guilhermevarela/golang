package main

import	(
	"fmt"
	"math"
)

type square struct {
	side float64
}

//another shape
type circle struct {
	radius float64
}

type shape interface {
	area() float64
}

func (z square) area() float64 {
	return z.side * z.side
}

//with implements the shape interface
func (c circle) area() float64 {
	return c.radius * c.radius * math.Pi
}

func info(z shape) {
	fmt.Println(z)
	fmt.Println(z.area())
}


func main() {
	s := square{side:10}
	c := circle{radius:5}
	info(s)
	info(c)
}