package main

import	(
	"fmt"
	"math"
)


//another shape
type circle struct {
	radius float64
}

type shape interface {
	area() float64
}

//with implements the shape interface
func (c circle) area() float64 {
	return c.radius * c.radius * math.Pi
}

func info(z shape) {
	fmt.Println("area",z.area())
}


func main() {

	c := circle{radius:5}
	info(&c)
}