package main

import	(
	"fmt"
	"math"
)


//another shape
type circle struct {
	radius float64
}

type shape interface {
	area() float64
}

//with implements the shape interface
func (c *circle) area() float64 {
	return c.radius * c.radius * math.Pi
}

func info(z shape) {
	fmt.Println("area",z.area())
}


func main() {

	c := circle{radius:5}
	info(c)
}

/*
./main.go:31: cannot use c (type circle) as type shape in argument to info:
        circle does not implement shape (area method has pointer receiver)

        POINTER RECEIVER CAN ONLY RECEIVE A POINTER
        Address might not exist --> ex; UNTYPED constant

        Receivers       Values
				-----------------------------------------------
				(t T)           T and *T
				(t *T)          *T

				Values          Receivers
				-----------------------------------------------
					T               (t T)
					*T              (t T) and (t *T)

 */