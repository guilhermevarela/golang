package main

import (
		"fmt"
		"sort"
		"strings"
)
type people []string

func (p people) Len() int {
	return len(p)
}

func (p people) Less( i, j int) bool {

		if strings.Compare(p[i],p[j]) == -1 {
			return true
		}
		return false
}

func (p people) Swap( i, j int)  {
		p[j], p[i] = p[i], p[j]
}



func main() {
	studyGroup := people{"Zeno", "John", "Al", "Jenny"}
	fmt.Println(studyGroup)
	sort.Sort(studyGroup)
	fmt.Println(studyGroup)


}