package main

import  (
		"fmt"
		"sort"
		"strings"
)

type sliceSorter struct{
	sl []string
}

func (ss *sliceSorter) Len() int {
	return len(ss.sl)
}

func (ss *sliceSorter) Less( i, j int) bool {

		if strings.Compare(ss.sl[i],ss.sl[j]) == -1 {
			return true
		}
		return false
}

func (ss *sliceSorter) Swap( i, j int)  {
		ss.sl[j], ss.sl[i] = ss.sl[i], ss.sl[j]
}



func main() {
	sl := []string{"Zeno", "John", "Al", "Jenny"}
	studyGroup := sliceSorter{
		sl:sl,
	}
	fmt.Println(studyGroup.sl)
	sort.Sort(&studyGroup)
	fmt.Println(studyGroup.sl)

}