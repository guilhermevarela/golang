package main

import  (
		"fmt"
		"sort"
)


func main() {
	sl := []string{"Zeno", "John", "Al", "Jenny"}

	fmt.Println(sl)
	sort.StringSlice(sl).Sort()

	fmt.Println(sl)

}