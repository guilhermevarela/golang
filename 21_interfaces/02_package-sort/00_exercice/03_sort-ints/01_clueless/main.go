package main

import  (
		"fmt"
		"sort"
)

type sliceSorter struct{
	sl []int
}

func (ss *sliceSorter) Len() int {
	return len(ss.sl)
}

func (ss *sliceSorter) Less( i, j int) bool {

		if ss.sl[i] < ss.sl[j] {
			return true
		}
		return false
}

func (ss *sliceSorter) Swap( i, j int)  {
		ss.sl[j], ss.sl[i] = ss.sl[i], ss.sl[j]
}



func main() {

	sl := []int{7, 4, 8, 2, 9, 19, 12, 32, 3}
	studyGroup := sliceSorter{
		sl:sl,
	}
	fmt.Println(studyGroup.sl)
	sort.Sort(&studyGroup)
	fmt.Println("ascending order:", studyGroup.sl)
	sort.Sort(sort.Reverse(&studyGroup))
	fmt.Println("descending order:",studyGroup.sl)

}