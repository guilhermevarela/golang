package main

import  (
		"fmt"
		"sort"
)


func main() {

	sl := sort.IntSlice{7, 4, 8, 2, 9, 19, 12, 32, 3}

	fmt.Println(sl)
	sl.Sort()
	fmt.Println("ascending order:", sl)
	sort.Sort(sort.Reverse(sl))
	fmt.Println("descending order:",sl)

}