package main

import  (
		"fmt"
		"sort"
)


func main() {

	sl := []int{7, 4, 8, 2, 9, 19, 12, 32, 3}

	fmt.Println(sl)
	sort.IntSlice(sl).Sort()
	fmt.Println("ascending order:", sl)
	sort.Sort(sort.Reverse(sort.IntSlice(sl)))
	fmt.Println("descending order:",sl)

}