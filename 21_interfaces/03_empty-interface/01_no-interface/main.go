package main

import "fmt"

type vehicle struct {
	Seats int
	MaxSpeed int
	Color string
}

type car struct {
	vehicle
	Wheels int
	Doors int
}

type plane struct {
	vehicle
	Jet bool
}

type boat struct {
	vehicle
	Length int
}


func main() {
	prius := car{}
	tacoma := car{}
	bmw528 := car{}
	cars := []car{prius, tacoma, bmw528}

	boing747 := plane{}
	boing757 := plane{}
	boing767 := plane{}
	planes := []plane{boing747,boing757, boing767}

	sanger := boat{}
	nautique := boat{}
	malibu := boat{}
	boats := []boat{sanger, nautique, malibu}

	for key, value := range cars {
		fmt.Println(key, " - ", value)
	}
	for key, value := range planes {
		fmt.Println(key, " - ", value)
	}
	for key, value := range boats {
		fmt.Println(key, " - ", value)
	}


}