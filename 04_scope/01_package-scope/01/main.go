package main

import "fmt"

var x int = 42

func main() {
	fmt.Println("This is main:",x)
	foo()
}

func foo() {
	fmt.Println("This is foo:",x)
}
