package main

import "fmt"

//wrapper is outer scope, x is always remenbered by wrapper
func wrapper() func() int {
	x := 0
	return func() int {
		x ++
		return x
	}
}




func main() {
	increment := wrapper() //function expressions
	fmt.Println(increment())
	fmt.Println(increment())
}

/*
closure helps us limit the scope of variables used by multiple functions
without closure, for two or more funcs to have access to the same variable,
that variable would need to be package scope
assigning a func to a variable
*/