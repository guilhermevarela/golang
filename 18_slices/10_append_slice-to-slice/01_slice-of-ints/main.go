package main

import "fmt"

func main() {

	mySlice := []int{1, 2, 3, 4}
	myOtherSlice := []int{5, 6, 7, 8, 9, 10, 11}

	//New slice cap -> 2*max(cap(slicea),cap(sliceb))
	//func append(slice []Type, elems ...Type) []Type
	mySlice = append(mySlice, myOtherSlice...)

	fmt.Println(mySlice)
	fmt.Println(len(mySlice))
	fmt.Println(cap(mySlice))
}