package main

import "fmt"

func main() {
	xs := make([]int, 0, 3)

	fmt.Println("------------------")
	fmt.Println(xs)
	fmt.Println(len(xs))
	fmt.Println(cap(xs))
	fmt.Println("------------------")
	for i := 0 ; i < 80; i++  {
		xs = append(xs, i)
		fmt.Println("len:", len(xs), "capacity:", cap(xs), "value:", xs[i])
	}
}