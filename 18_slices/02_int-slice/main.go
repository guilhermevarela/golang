package main

import "fmt"

func main() {
	xs := []int{1,2,3,5,7,11}
	for i, v := range xs {
		fmt.Println(i, " - ", v)
	}
}